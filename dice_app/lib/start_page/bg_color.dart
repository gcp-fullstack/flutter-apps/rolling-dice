import 'package:dice_app/start_page/app_bar_design.dart';
import 'package:dice_app/start_page/rolling_dice.dart';
import 'package:flutter/material.dart';

class BodyContainerDesign extends StatelessWidget {
  const BodyContainerDesign({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: const AppBarDesign(
          appTitle: "Rolling Dice App",
        ),
        body: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromARGB(255, 229, 141, 245),
                Color.fromARGB(255, 104, 21, 247)
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
          ),
          child: const Center(
            child: RollingDiceLogic(),
          ),
        ),
      ),
    );
  }
}
