import 'dart:math';

import 'package:flutter/material.dart';

class RollingDiceLogic extends StatefulWidget {
  const RollingDiceLogic({super.key});

  @override
  State<StatefulWidget> createState() {
    return _RollingDiceLogicState();
  }
}

class _RollingDiceLogicState extends State<RollingDiceLogic> {
  var activeImage = 2;

  void rollDice() {
    setState(() {
      activeImage = Random().nextInt(6) + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Image.asset("assets/images/dice-$activeImage.png", width: 200),
        const SizedBox(
          height: 20,
        ),
        OutlinedButton(
          onPressed: rollDice,
          style: OutlinedButton.styleFrom(
            backgroundColor: Colors.white,
          ),
          child: const Text(
            "Roll Dice",
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    );
  }
}
